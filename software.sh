#!/bin/bash
# purpose: baseline software install script
# created: Aaron Francis (imome9a)

apps='neofetch tree filezilla peek dialog libfuse2t64'
snaps='chromium'
snaps_classic='codium'
manual='Etcher (AppImage), RustDesk (.deb), VirtualBox (.deb)'

echo
echo --------------------------------------------------------------------
echo installing: latest updates
echo --------------------------------------------------------------------
echo

sudo apt update && sudo apt upgrade -y

echo
echo --------------------------------------------------------------------
echo installing: $apps $snaps $snaps_classic
echo --------------------------------------------------------------------
echo

sudo apt install $apps -y
sudo snap install $snaps
sudo snap install $snaps_classic --classic

echo
echo --------------------------------------------------------------------
echo installing: $manual
echo --------------------------------------------------------------------
echo

mkdir ~/Scripts
sudo mkdir /usr/local/bin/appimages

# Etcher
echo installing Etcher
wget https://github.com/balena-io/etcher/releases/download/v1.19.21/balenaEtcher-1.19.21-x64.AppImage

etcher=$(ls | grep balena* 2>&1)

sudo mkdir /usr/local/bin/appimages/etcher
sudo mv balenaEtcher* /usr/local/bin/appimages/etcher
sudo chmod +x /usr/local/bin/appimages/etcher/balenaEtcher*
sudo /usr/local/bin/appimages/etcher/balenaEtcher* --appimage-extract
sudo cp squash*/*.desktop /usr/share/applications
sudo mv squash* /usr/local/bin/appimages/etcher
sudo chmod 644 /usr/share/applications/balenaEtcher*.desktop
sudo sed -i "s|Exec=balena-etcher|Exec=/usr/local/bin/appimages/etcher/$etcher|" /usr/share/applications/balenaEtcher*.desktop
sudo sed -i "s|Icon=balena-etcher|Icon=/usr/local/bin/appimages/etcher/squashfs-root/balena-etcher.png|" /usr/share/applications/balenaEtcher*.desktop


# RustDesk
echo installing RustDesk
wget https://github.com/rustdesk/rustdesk/releases/download/1.2.3-2/rustdesk-1.2.3-2-x86_64.deb
sudo apt install ./rustdesk-1.2.3-2-x86_64.deb

# echo installing RustDesk
# wget https://github.com/rustdesk/rustdesk/releases/download/1.2.3-2/rustdesk-1.2.3-2-x86_64.AppImage

# rustdesk=$(ls | grep rust* 2>&1)

# sudo mkdir /usr/local/bin/appimages/rustdesk
# sudo mv rust* /usr/local/bin/appimages/rustdesk
# sudo chmod +x /usr/local/bin/appimages/rustdesk/rust*
# sudo /usr/local/bin/appimages/rustdesk/rust* --appimage-extract
# sudo cp squash*/*.desktop /usr/share/applications
# sudo mv squash* /usr/local/bin/appimages/rustdesk
# sudo chmod 644 /usr/share/applications/rust*.desktop
# sudo sed -i "s|Exec=usr/lib/rustdesk/rustdesk|Exec=/usr/local/bin/appimages/rustdesk/$rustdesk|" /usr/share/applications/rust*.desktop
# sudo sed -i "s|Icon=rustdesk|Icon=/usr/local/bin/appimages/rustdesk/squashfs-root/rustdesk.svg|" /usr/share/applications/rust*.desktop

# VirtualBox 7.0
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/oracle-virtualbox-2016.gpg] https://download.virtualbox.org/virtualbox/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") contrib" | \
  sudo tee /etc/apt/sources.list.d/virtualbox.list > /dev/null

wget https://www.virtualbox.org/download/oracle_vbox_2016.asc
sudo gpg --yes --output /usr/share/keyrings/oracle-virtualbox-2016.gpg --dearmor oracle_vbox_2016.asc
sudo apt-get update && sudo apt-get install virtualbox-7.0 -y
sudo usermod -aG vboxusers ubuntu

wget https://download.virtualbox.org/virtualbox/7.0.18/Oracle_VM_VirtualBox_Extension_Pack-7.0.18.vbox-extpack
echo y | sudo vboxmanage extpack install ./Oracle_VM_VirtualBox_Extension_Pack-7.0.18.vbox-extpack

# Cisco PacketTracer 8.2.2
wget https://filedn.com/l0sMlKIoYFh4VYbRP5SLFzR/software/CiscoPacketTracer822_amd64_signed.deb
wget http://archive.ubuntu.com/ubuntu/pool/universe/m/mesa/libgl1-mesa-glx_23.0.4-0ubuntu1~22.04.1_amd64.deb

sudo apt install libxcb-xinerama0-dev -y
sudo dpkg -i libgl1-mesa-glx_23.0.4-0ubuntu1~22.04.1_amd64.deb
sudo dpkg -i CiscoPacketTracer822_amd64_signed.deb

echo
echo --------------------------------------------------------------------
echo Bash  setup...
echo --------------------------------------------------------------------
echo

mv ~/ubuntu/update.sh ~/Scripts
sudo chmod +x ~/Scripts/update.sh
echo \
  "# local aliases
   alias update='~/Scripts/update.sh'"\
  >> ~/.bashrc

source ~/.bashrc

echo
echo --------------------------------------------------------------------
echo Adding shortcuts...
echo --------------------------------------------------------------------
echo

dconf write /org/gnome/shell/favorite-apps "['firefox_firefox.desktop', 'chromium_chromium.desktop', 'codium_codium.desktop', 'cisco-pt821.desktop', 'virtualbox.desktop', 'org.gnome.Terminal.desktop', 'org.gnome.Nautilus.desktop']"

echo
echo --------------------------------------------------------------------
echo Cleaning up...
echo --------------------------------------------------------------------
echo

rm -rf ~/Music ~/Public ~/Templates ~/Videos
rm -rf ~/ubuntu oracle_vbox* Oracle_VM_VirtualBox* CiscoPacketTracer* libgl*

echo
echo --------------------------------------------------------------------
echo rebooting...
echo --------------------------------------------------------------------
echo

sudo reboot
#!/bin/bash

echo
echo ---------------------------------
echo !!!!!!!!!!! updating !!!!!!!!!!!!
echo ---------------------------------
echo

sudo apt update

echo
echo ---------------------------------
echo !!!!!!!!!!! upgrading !!!!!!!!!!!
echo ---------------------------------
echo

sudo apt upgrade -y

echo
echo ---------------------------------
echo !!!!!!!!!!! cleaning !!!!!!!!!!!!
echo ---------------------------------
echo

sudo apt autoclean

echo
echo ---------------------------------
echo !!!!!!!!!!! removing !!!!!!!!!!!!
echo ---------------------------------
echo

sudo apt autoremove -y
